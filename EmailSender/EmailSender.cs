﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace EmailSender
{
    public partial class EmailSender : Form
    {
        DataSet ds = null;
        OpenFileDialog excelOpenDialog = new OpenFileDialog();
        OpenFileDialog emailContentOpenDialog = new OpenFileDialog();
        StringBuilder sb = new StringBuilder();
        int progressBarMaxValue;

        public EmailSender()
        {
            InitializeComponent();
        }

        private void btnBrowseExcel_Click(object sender, EventArgs e)
        {
            Stream stream = null;
            excelOpenDialog.Title = "Open";
            excelOpenDialog.Filter = "Microsoft Excel 2007-2013 XML (*.xlsx)|*.xlsx|"
                + "Microsoft Excel 97-2003 (*.xls)|*.xls|"
                + "CSV Files (*.csv)|*.csv|"
                + "TSV Files (*.tsv)|*.tsv|"
                + "All Files (*.*)|*.*";
            excelOpenDialog.FilterIndex = 0;

            if (excelOpenDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((stream = excelOpenDialog.OpenFile()) != null)
                    {
                        txtExcelFile.Text = excelOpenDialog.FileName;

                        using (stream)
                        {
                            switch (Path.GetExtension(excelOpenDialog.FileName))
                            {
                                case ".xlsx":
                                case ".xls":
                                    ds = new FileReader.ExcelFileReader(stream).Read();
                                    break;
                                default:
                                    MessageBox.Show("File format not supported.");
                                    break;
                            }
                        }

                        if (ds != null)
                            CalculateProgress(ds);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                    excelOpenDialog.Dispose();
                }
            }
        }

        private void CalculateProgress(DataSet Dataset)
        {
            foreach (DataTable dt in Dataset.Tables)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    lstName.Items.Add(column.ColumnName);
                    lstEmail.Items.Add(column.ColumnName);
                }
                progressBarMaxValue += dt.Rows.Count;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(excelOpenDialog.FileName))
            {
                MessageBox.Show("Please select excel file.");
                return;
            }

            if (string.IsNullOrEmpty(emailContentOpenDialog.FileName))
            {
                MessageBox.Show("Please select email template file.");
                return;
            }

            this.progressBar.Visible = true;
            this.progressBar.Maximum = progressBarMaxValue;

            try
            {
                if (lstName.SelectedItem == null && 
                    Convert.ToBoolean(ConfigurationSettings
                    .AppSettings["SendWithRecepientName"]))
                {
                    MessageBox.Show("Please select recepient name field.");
                    return;
                }

                if (lstEmail.SelectedItem == null)
                {
                    MessageBox.Show("Please select recepient email field.");
                    return;
                }

                if (!this.backgroundWorker.IsBusy)
                {
                    this.btnSend.Enabled = false;
                    backgroundWorker.RunWorkerAsync(new Input()
                    {
                        RecepientNameFieldName = (lstName.SelectedItems.Count == 1) ? 
                        lstName.SelectedItem.ToString() : string.Empty,
                        RecepientEmailFieldName = lstEmail.SelectedItem.ToString(),
                        DataSet = ds,
                        EmailContent = sb
                    });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EmailSender_Load(object sender, EventArgs e)
        {
            this.AcceptButton = this.btnSend;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            string name = string.Empty;
            string email = null;

            try
            {
                BackgroundWorker sendingWorker = (BackgroundWorker)sender;

                if (!sendingWorker.CancellationPending) // Check if there is a cancellation request pending 
                {
                    Input input = (Input)e.Argument;

                    if (input.RecepientEmailFieldName != null && input.RecepientEmailFieldName.Length > 0)
                    {
                        EmailEngine emailEngine = new EmailEngine();
                        foreach (DataTable dt in input.DataSet.Tables)
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                if (!string.IsNullOrEmpty(input.RecepientNameFieldName))
                                    name = row.Field<string>(input.RecepientNameFieldName);

                                email = row.Field<string>(input.RecepientEmailFieldName);
                                if (!string.IsNullOrEmpty(email))
                                {
                                    string emailContent = input.EmailContent.ToString();
                                    if (Convert.ToBoolean(ConfigurationSettings.AppSettings["Send_With_Recepient_Name"]))
                                    {
                                        emailContent = emailContent.Replace(System.Configuration.ConfigurationSettings
                                            .AppSettings["Recepient_Name_Container"], name);
                                    }
                                    else
                                    {
                                        emailContent = emailContent.Replace(System.Configuration.ConfigurationSettings
                                            .AppSettings["Recepient_Name_Container"], "");
                                    }

                                    if (ConfigurationSettings.AppSettings["Mail_Service"].Equals("gmail", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        emailEngine.SendMail(ConfigurationSettings.AppSettings["Gmail_Sender_Email"], name,
                                            email, null, ConfigurationSettings.AppSettings["Subject"], null,
                                            emailContent, ConfigurationSettings.AppSettings["Gmail_Sender_Email"],
                                            ConfigurationSettings.AppSettings["Gmail_Sender_Email_Password"],
                                            Convert.ToBoolean(ConfigurationSettings.AppSettings["Enable_Encryption"]));
                                    }
                                    else
                                    {
                                        emailEngine.SendAsyncMailViaSendGrid(ConfigurationSettings.AppSettings["SendGrid_API_Key"],
                                            ConfigurationSettings.AppSettings["SendGrid_Email"], name,
                                            email, null, ConfigurationSettings.AppSettings["Subject"], null,
                                            emailContent, ConfigurationSettings.AppSettings["SendGrid_Username"],
                                            ConfigurationSettings.AppSettings["SendGrid_Password"],
                                            Convert.ToBoolean(ConfigurationSettings.AppSettings["Enable_Encryption"])).Wait();
                                    }
                                }
                                sendingWorker.ReportProgress(1);
                                name = null;
                            }
                        }
                    }
                }
                e.Result = true;
            }
            catch (Exception ex)
            {
                e.Result = false;
                ex.Source = email;
                Utils.WriteErrorLog("Error occured while sending email to " + email, ex);
                throw ex;
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (!e.Cancelled && e.Error == null)//Check if the worker has been canceled or if an error occurred
                {
                    DialogResult dr = MessageBox.Show("Email sent successfully." +
                            Environment.NewLine + "Do you want to exit?", "Confirm",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                    if (dr == DialogResult.Yes)
                    {
                        System.Threading.Thread.Sleep(2000);
                        Environment.Exit(0);
                    }
                }
                else if (e.Cancelled)
                {
                    MessageBox.Show("Cancelled");
                }
                else
                {
                    MessageBox.Show("An error has occurred while sending mail to " + e.Error.Source + Environment.NewLine + e.Error.Message);
                }
                this.btnSend.Enabled = true;//Re enable the proceed button
                this.progressBar.Value = 0;
                this.progressBar.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (this.progressBar.Value + e.ProgressPercentage >= progressBar.Maximum)
                this.progressBar.Value = this.progressBar.Maximum;
            else
                this.progressBar.Value += e.ProgressPercentage;
        }

        private void btnBrowseTemplate_Click(object sender, EventArgs e)
        {
            Stream stream = null;
            emailContentOpenDialog.Title = "Open";
            emailContentOpenDialog.Filter = "Text (*.txt)|*.txt|"
                + "All Files (*.*)|*.*";
            emailContentOpenDialog.FilterIndex = 0;

            if (emailContentOpenDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((stream = emailContentOpenDialog.OpenFile()) != null)
                    {
                        txtTemplate.Text = emailContentOpenDialog.FileName;

                        using (stream)
                        {
                            switch (Path.GetExtension(emailContentOpenDialog.FileName))
                            {
                                case ".txt":
                                    sb.Append(new StreamReader(stream).ReadToEnd());
                                    break;
                                default:
                                    MessageBox.Show("File format not supported.");
                                    break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                    emailContentOpenDialog.Dispose();
                }
            }
        }

        private class Input
        {
            public string RecepientNameFieldName { get; set; }
            public string RecepientEmailFieldName { get; set; }
            public DataSet DataSet { get; set; }
            public StringBuilder EmailContent { get; set; }
        }
    }
}
