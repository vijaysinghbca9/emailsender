﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
//using NPOI.XSSF.UserModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace FileReader
{
    public class ExcelFileReader : IFileReader<string, DataSet>
    {
        public string FileName { get; set; }
        public Stream Stream { get; set; }

        public ExcelFileReader() { }
        public ExcelFileReader(string FileName)
            : this (new FileStream(FileName, FileMode.Open, FileAccess.Read))
        {
            this.FileName = FileName;
        }
        public ExcelFileReader(Stream stream)
        {
            this.Stream = stream;
        }

        public DataSet Read()
        {
            try
            {
                return GetDataSetFromExcelXML();
            }
            catch
            {
                return GetDataSetFromExcel();
            }
        }

        private DataSet GetDataSetFromExcelXML()
        {
            try
            {
                DataSet ds = new DataSet();
                using (var excelPkg = new OfficeOpenXml.ExcelPackage())
                {
                    excelPkg.Load(Stream);

                    foreach (ExcelWorksheet ws in excelPkg.Workbook.Worksheets)
                    {
                        DataTable dataTable = new DataTable();
                        foreach (var headerCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                        {
                            dataTable.Columns.Add(headerCell.Text);
                        }

                        for (int rowNum = 2; rowNum <= ws.Dimension.End.Row; rowNum++)
                        {
                            var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                            DataRow row = dataTable.Rows.Add();
                            foreach (var cell in wsRow)
                            {
                                row[cell.Start.Column - 1] = cell.Text;
                            }
                        }
                        dataTable.TableName = ws.Name;
                        ds.Tables.Add(dataTable);
                    }
                    return ds;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (!string.IsNullOrEmpty(FileName))
                    Stream.Close();
            }
        }

        private DataSet ExcelToDataTable()
        {
            DataSet ds = new DataSet();
            string strConn = string.Empty;
            IWorkbook workbook;
            //FileInfo file = new FileInfo(FileName);
            //if (!file.Exists) { throw new Exception("Error, file doesn't exists!"); }

            switch (".xls")
            {
                case ".xls":
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Stream + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;'";
                    workbook = new HSSFWorkbook(Stream);
                    break;
                case ".xlsx":
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Stream + ";Extended Properties='Excel 12.0;HDR=Yes;IMEX=1;'";
                    //workbook = new XSSFWorkbook(Stream);
                    break;
                default:
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Stream + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;'";
                    break;
            }
            System.Data.OleDb.OleDbConnection cnnxls = new System.Data.OleDb.OleDbConnection(strConn);
            for (int i = 0; i < workbook.NumberOfSheets; i++)
            {
                ISheet sheet = workbook.GetSheetAt(i); // zero-based index of your target sheet
                DataTable dt = new DataTable(sheet.SheetName);
                System.Data.OleDb.OleDbDataAdapter oda = new System.Data.OleDb
                    .OleDbDataAdapter(string.Format("select * from [{0}$]", sheet.SheetName), cnnxls);
                oda.Fill(dt);
                ds.Tables.Add(dt);
            }
            return ds;
        }

        private DataSet GetDataSetFromExcel()
        {
            try
            {
                DataSet ds = new DataSet();
                IWorkbook workbook;
                using (Stream)
                {
                    workbook = new HSSFWorkbook(Stream);
                    for (int i = 0; i < workbook.NumberOfSheets; i++)
                    {
                        ISheet sheet = workbook.GetSheetAt(i); // zero-based index of your target sheet
                        DataTable dt = new DataTable(sheet.SheetName);
                        IRow headerRow = sheet.GetRow(0);
                        foreach (ICell headerCell in headerRow)
                        {
                            dt.Columns.Add(headerCell.ToString());
                        }

                        int rowIndex = 0;
                        foreach (IRow row in sheet)
                        {
                            if (rowIndex++ == 0) continue;
                            DataRow dr = dt.NewRow();
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                dr[j] = row.GetCell(j);
                            }
                            dt.Rows.Add(dr);
                        }
                        dt.TableName = sheet.SheetName;
                        ds.Tables.Add(dt);
                    }
                    return ds;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (Stream != null)
                    Stream.Close();
            }
        }
    }
}
