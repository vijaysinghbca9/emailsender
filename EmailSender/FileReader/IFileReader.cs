﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileReader
{
    interface IFileReader<TInput, TOutput>
    {
        TInput FileName { get; set; }
        TOutput Read();
    }
}
