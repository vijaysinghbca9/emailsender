﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace EmailSender
{
    class EmailEngine
    {
        //Gmail SMTP settings with TLS enabled
        public const string GMAIL_SMTP_HOST = "smtp.gmail.com";
        public const int GMAIL_SMTP_PORT = 587;

        //Yahoo SMTP settings with SSL enabled
        public const string YAHOO_SMTP_MAIL_HOST = "smtp.mail.yahoo.com";
        public const int YAHOO_SMTP_MAIL_PORT = 995;

        //Yahoo SMTP settings without SSL enabled
        public const string YAHOO_SMTP_BIZMAIL_HOST = "smtp.bizmail.yahoo.com";
        public const int YAHOO_SMTP_BIZMAIL_PORT = 25;

        private SmtpClient _smtpClient;
        private MailMessage _mailMessage;
        private NetworkCredential _credentials;
        private int _maxAttempts = 3;
        private static SendGridClient client = null;
        
        public EmailEngine() : this(
            ConfigurationSettings.AppSettings["SMTP_Host"], 
            Convert.ToInt32(ConfigurationSettings.AppSettings["SMTP_Port"])) { }

        public EmailEngine(String Host, int PortNumber)
        {
            SMTPClient = new SmtpClient(Host, PortNumber);
        }

        public void SetMailParametres(String From, String displayName, String To, String Cc, 
            String Subject, String Attachment, String Body, String User, String Password)
        {
            Message = new MailMessage();
            Message.From = new MailAddress(From, displayName);
            if ((To != null) && !To.Equals(String.Empty))
            {
                foreach (string str in To.Split(';'))
                    Message.To.Add(str.Trim());
            }
            if ((Cc != null) && !Cc.Equals(String.Empty))
            {
                foreach (string str in Cc.Split(';'))
                    Message.CC.Add(str.Trim());
            }
            Message.Subject = Subject;
            Message.Body = Body;
            Message.IsBodyHtml = Convert.ToBoolean(ConfigurationSettings.AppSettings["Is_Body_Html"]);
            if ((Attachment != null) && !Attachment.Equals(String.Empty))
                Message.Attachments.Add(new System.Net.Mail.Attachment(Attachment));
            Credentials = new NetworkCredential(User, Password);
        }

        public bool Send(bool EnableEncryption)
        {
            int tries = 0;
            while (true)
            {
                try
                {
                    SMTPClient.EnableSsl = EnableEncryption;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s,
                        System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };
                    SMTPClient.Timeout = 21000;
                    string smtpTimeOut = ConfigurationSettings.AppSettings["SMTP_TimeOut"];
                    if (!string.IsNullOrEmpty(smtpTimeOut) && Utils.IsNumeric(smtpTimeOut))
                    {
                        SMTPClient.Timeout = Convert.ToInt32(smtpTimeOut);
                    }
                    SMTPClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SMTPClient.UseDefaultCredentials = false;
                    SMTPClient.Credentials = Credentials;
                    Utils.WriteDebugLog("Trying to send mail.");
                    SMTPClient.Send(Message);
                    Utils.WriteDebugLog("Mail sent successfully.");
                    Message.AlternateViews.Dispose();
                    Message.Attachments.Dispose();
                    Message.Dispose();
                    break;
                }
                catch (SmtpFailedRecipientsException ex)
                {
                    for (int i = 0; i < ex.InnerExceptions.Length; i++)
                    {
                        SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                        if (status == SmtpStatusCode.MailboxBusy ||
                            status == SmtpStatusCode.MailboxUnavailable)
                        {
                            if (++tries >= MaxAttemptsAllowed) throw;
                            else
                            {
                                Utils.WriteErrorLog("Delivery failed - retrying in " + (5 * tries) + " seconds.", ex);
                                System.Threading.Thread.Sleep(5000 * tries);
                            }
                        }
                        else
                        {
                            Utils.WriteErrorLog("Failed to deliver message to " +
                                ex.InnerExceptions[i].FailedRecipient, ex);
                            throw;

                        }
                    }
                }
                catch (SmtpException ex)
                {
                    if (++tries >= MaxAttemptsAllowed) throw;
                    else
                    {
                        Utils.WriteErrorLog("Delivery failed - retrying in " + (5 * tries) + " seconds.", ex);
                        System.Threading.Thread.Sleep(5000 * tries);
                    }
                }
                catch (Exception ex)
                {
                    if (++tries >= MaxAttemptsAllowed) throw;
                    else
                    {
                        Utils.WriteErrorLog("Delivery failed - retrying in " + (5 * tries) + " seconds.", ex);
                        System.Threading.Thread.Sleep(5000 * tries);
                    }
                }
            }
            return true;
        }

		public bool SendMail(String From, String displayName, String To, String Cc, 
            String Subject, String Attachment, String Body, String User, String Password, bool EnableEncryption)
        {
            SetMailParametres(From, displayName, To, Cc, Subject, Attachment, Body, User, Password);
            return Send(EnableEncryption);
        }

        public void SendAsyncMail(String From, String displayName, String To, String Cc,
            String Subject, String Attachment, String Body, String User, String Password, bool EnableEncryption)
        {
            // Spawn a new thread for sending mails, the mail sending process should not make the 
            // end user wait for the response if any error occurs.
            ThreadStart starter = delegate 
            {
                SetMailParametres(From, displayName, To, Cc, Subject, Attachment, Body, User, Password);
                Send(EnableEncryption);
            };
            Thread thread = new Thread(starter);
            thread.IsBackground = true;
            thread.Start();
        }

        public async Task SendAsyncMailViaSendGrid(String SendGridAPIKey, String From, String DisplayName, String To, String Cc,
            String Subject, String Attachment, String Body, String User, String Password, bool EnableEncryption)
        {
            if (client == null) client = new SendGridClient(SendGridAPIKey);
            var from = new EmailAddress(From, DisplayName);
            var to = new EmailAddress(To, "");
            var msg = MailHelper.CreateSingleEmail(from, to, Subject, null, Body);
            var response = await client.SendEmailAsync(msg);
        }

        public SmtpClient SMTPClient
        {
            get { return _smtpClient; }
            set { _smtpClient = value; }
        }

        public MailMessage Message
        {
            get { return _mailMessage; }
            set { _mailMessage = value; }
        }

        public NetworkCredential Credentials
        {
            get { return _credentials; }
            set { _credentials = value; }
        }

        public int MaxAttemptsAllowed
        {
            get { return _maxAttempts; }
            set { _maxAttempts = value; }
        }
    }
}
