﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace EmailSender
{
    public class Utils
    {
        private static ILog _logger = LogManager.GetLogger("EmailEgine");

        public static bool IsNumeric(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            Regex objNumeric = new Regex("[^0-9]");
            return !objNumeric.IsMatch(value);
        }

        public static void WriteDebugLog(string str)
        {
            if (_logger.IsDebugEnabled)
                _logger.Debug(str);
        }

        public static void WriteErrorLog(string str, Exception ex)
        {
            if (_logger.IsErrorEnabled)
                _logger.Error(str, ex);
        }
    }
}
